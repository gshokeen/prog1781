﻿/*
 *Progran Name : A2GarimaSinghP1
 * 
 * Revision History : 2020-02-16
 * 
 * Created by Garima Singh
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A2GarimaSinghP2
{
    class Program
    {
        public static void VolumeCalculate(Double radius)
        {
            //For sphere
            Double volume;
            volume = (4 / 3) * (3.14159) * radius * radius * radius;
            Console.WriteLine("Volume of sphere = "+volume);
        }
        public static void VolumeCalculate(Double length, Double width, Double height)
        {
            //For rectangular prism
            Double volume;
            volume = length * width * height;
            Console.WriteLine("Volume of rectangular prism = " + volume);
        }
        public static void VolumeCalculate(Double radius , Double height)
        {
            //For cylinder
            Double volume;
            volume = (3.14159) * radius * radius * height;
            Console.WriteLine("Volume of cylinder = "+volume);
        }
        static void Main(string[] args)
        {
            //Variable declaration
            String choice;
            int choiceInteger;
            String radius;
            Double radiusDouble;
            String height;
            Double heightDouble;
            String length;
            String width;
            Double lengthDouble;
            Double widthDouble;

            
            Console.WriteLine("What shape do you want to make ? \nEnter 1 for sphere \nEnter 2 for cylinder \nEnter 3 for rectangular prism");
            Console.Write("Enter your choice : ");
            choice = Console.ReadLine();
            choiceInteger = int.Parse(choice);

            switch (choiceInteger)
            {
                
                //For sphere
                case 1: Console.Write("Enter the radius of the sphere : ");
                    radius = Console.ReadLine();
                    radiusDouble = Double.Parse(radius);
                    VolumeCalculate(radiusDouble);
                    break;
                
                //For cylinder
                case 2: Console.Write("Enter the radius of the cylinder : ");
                    radius = Console.ReadLine();
                    radiusDouble = Double.Parse(radius);
                    Console.Write("Enter the height of the cylinder : ");
                    height = Console.ReadLine();
                    heightDouble = Double.Parse(height);
                    VolumeCalculate(radiusDouble , heightDouble);
                    break;
                
                //For rectangular prism
                case 3: Console.Write("Enter the length of the rectangular prism : ");
                    length = Console.ReadLine();
                    lengthDouble = Double.Parse(length);
                    Console.Write("Enter the width of the rectangular prism : ");
                    width = Console.ReadLine();
                    widthDouble = Double.Parse(width);
                    Console.Write("Enter the height of the rectangular prism : ");
                    height = Console.ReadLine();
                    heightDouble = Double.Parse(height);
                    VolumeCalculate(lengthDouble , widthDouble , heightDouble);
                    break;
                
                //For wrong choices
                default : Console.WriteLine("Wrong choice \n only choices were from 1 , 2 & 3");
                    break;
            }
            Console.ReadKey();
        }
    }
}
